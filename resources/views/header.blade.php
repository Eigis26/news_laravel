<body>
    <div class="header">
        <a style="text-decoration: none;" href="{{url('/')}}"><h1 class="header_title_deg">NEWS</h1></a>
        
    </div>
    <div class="mode">
        <div class="form-check form-switch">
            @if(Cookie::get('mode') == "dark")
            <a href="{{url('/mode_on')}}" class="btn btn-success">Night mode--ON</a>
            @else
            <a href="{{url('/mode')}}" class="btn btn-success">Night mode--OFF</a>
            @endif
          </div>
    </div>
</body>