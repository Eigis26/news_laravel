<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>News</title>
</head>
<body>
    {{-- Style bootstrap5 --}}
    @include('css')
    {{-- Header --}}
    @include('header')
    {{-- Blogs section --}}
    @include('blogs')
    {{-- Articles bottom section --}}
    @include('articles')


</body>
</html>