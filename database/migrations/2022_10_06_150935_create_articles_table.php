<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->string('author')->nullable()->default('');
            $table->string('title')->nullable()->default('');
            $table->longText('description')->nullable();
            $table->longText('url')->nullable()->default('https://');
            $table->longText('urlToImage')->nullable()->default('');
            $table->string('publishedAt')->nullable()->default('');
            $table->longText('content')->nullable()->default('');
            $table->string('img')->nullable()->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
};
