##Installation 

1. php composer install
2. create env file
3. create db
4. copy and paste all data from example env
5. insert database data to .env
6. php artisan migrate
7. php artisan key:generate
8. php artisan serve
9. go to http://127.0.0.1:8000 