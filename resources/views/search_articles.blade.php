
<div id="articles_back" class="row pt-3 text-center">
    @include('css')
    @include('header')
    <div class="col text-center pt-3">
        <form action="{{url('search')}}" method="GET">
            @csrf
            <input name="search" type="text" placeholder="Article search">
            <input type="submit" value="Search" class="btn btn-primary">
        </form>
    </div>

@forelse($article as $articles)
<div class="card bg-dark text-white mb-3 mt-3">
    <a style="text-decoration: none; color:white" href="{{url('blog', $articles->id)}}">
    @if($articles->urlToImage != null)
        <img height="500" src="{{$articles->urlToImage}}" class="card-img" alt="{{$articles->title}}">
    @else
        <img height="500" src="img/No_image_available.png" class="card-img" alt="{{$articles->title}}">
    @endif
    <div class="row">
        <div class="col">
            <h3 class="ms-5" style="font-weight: 700">{{$articles->title}}</h3>
        </div>
    </div>
  </div></a>
  @empty
  <div class="articles_title_deg">
      <h5 style="margin-left: 10px" class="card-title">No data found</h5>
  </div>
@endforelse