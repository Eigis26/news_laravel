<?php

namespace App\Http\Controllers;

use App\Models\Articles;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return Articles::get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $article = new Articles;
        $date = date('Y-m-d H:i:s');
        $article -> author = $request -> author;
        $article -> title = $request -> title;
        $article -> description = $request -> description;
        $article -> url = $request -> url;
        $article -> urlToImage = $request->urlToImage;
        $article -> publishedAt = $date;
        $article -> content = $request -> content;
        $article -> save();
        return $article;
       
    }

    public function show($id)
    {
       $articleId = Articles::find($id);
       return $articleId;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $article =Articles::find($id);
        $date = date('Y-m-d H:i:s');
        $article -> author = $request -> author;
        $article -> title = $request -> title;
        $article -> description = $request -> description;
        $article -> url = $request -> url;
        $article -> urlToImage = $request->urlToImage;
        $article -> publishedAt = $date;
        $article -> content = $request -> content;
        $image=$request->urlToImage;
        $article -> save();
            return $article;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $del = Articles::destroy($id);

        return redirect(url('/'));
        // return $del;
    }
    
}
