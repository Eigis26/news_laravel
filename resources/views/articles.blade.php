<div id="articles_back" class="row">
        <div class="container">
            <div class="row">
                <div class="col pb-3 pt-5">
                    <h3 class="ms-5 text-center" id="search_ttl" style="font-weight: 700">Search for articles</h3>
                </div>
            </div>
            <div class="row">
                <div class="col text-center pb-3">
                    <form action="{{url('search')}}" method="get">
                        @csrf
                        <input name="search" type="text" placeholder="Article search">
                        <input type="submit" value="Search" class="btn btn-primary">
                    </form>
                </div>
                <a href="{{url('create_article')}}" class="btn btn-primary">Create Article</a>
            </div>
        </div>
        <?php $count = 0; ?>
        @forelse($article as $articles)
        <?php
            if($count == 30) break; 
        ?>
            <div class="card bg-dark text-white mb-3 mt-3 p-5">
                <a style="text-decoration: none; color:white" href="{{url('show_article', $articles->id)}}">
                 @if($articles->urlToImage != null)
                <img height="500" src="{{$articles->urlToImage}}" class="card-img" alt="...">
                @elseif($articles->urlToImage === null)
                <img height="500" src="img/No_image_available.png" class="card-img" alt="...">
                @else
                <img height="500" src="{{$articles->img}}" class="card-img" alt="...">
                @endif
                <div class="row">
                    <div class="col">
                        <h3 class="ms-5" style="font-weight: 700">{{$articles->title}}</h3>
                    </div>
                </div>
              </div></a>
              <?php $count ++; ?>
              @empty
              <div class="articles_title_deg">
                  <h5 style="margin-left: 10px" class="card-title">No data found</h5>
              </div>

        @endforelse
    </div>
