<link href="{{asset('https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css')}}" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
@if (Cookie::get('mode') == "dark")
<link rel="stylesheet" type="text/css" href="{{asset('night.css')}}" />
@else
<link rel="stylesheet" type="text/css" href="{{asset('style.css')}}" />
@endif
