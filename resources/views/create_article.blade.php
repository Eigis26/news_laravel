@include('css')

<body id="create_art">
    <div id="create_art" class="row">
        <div class="col pt-4 ms-3">
          @if(session()->has('message'))
              <div class="alert alert-success">
                  {{session()->get('message')}}
              </div>
          @endif
    <a class="btn btn-success " href="{{url('/')}}">Back</a>
        </div>
</div>
<div class="row">
    <div class="col text-center">
        <h1 id="create_art">Create article</h1>
    </div>
</div>
<form id="create_art"  method="post" action="{{url('article')}}" enctype="multipart/form-data">
    @csrf
    <div id="create_art" class="mb-3  mx-5">
      <label for="author" class="form-label">Author :</label>
      <input type="text" class="form-control" name="author">
    </div>
    <div id="create_art"  class="mb-3  mx-5">
        <label for="title" class="form-label">Title :</label>
        <input type="text" class="form-control" name="title">
      </div>
      <div id="create_art"  class="mb-3  mx-5">
        <label for="description" class="form-label">Description :</label>
        <input type="text" class="form-control" name="description">
      </div>
      <div id="create_art"  class="mb-3  mx-5">
        <label for="url" class="form-label">Url to Website :</label>
        <input type="text" class="form-control" name="url" value="https://">
      </div>
      <div id="create_art"  class="mb-3  mx-5">
        <label class="form-label">Image :  </label>
        <input type="file" name="urlToImage" required>
      </div>
      <div id="create_art"  class="mb-3  mx-5">
        <label for="content" class="form-label">Content :</label>
        <input type="text" class="form-control" name="content">
      </div>
    <button type="submit" class="btn btn-primary ms-3 mb-3">Submit</button>
  </form>
</body>