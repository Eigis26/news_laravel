@include('css')
    <div id="bg_title" class="row">
        <div class="col pt-4">
    <a class="btn btn-success ms-5" href="{{url('/')}}">Back</a>
        </div>

    <div class="row">
            <div class="card bg-dark text-white mb-3 mt-3 p-5">
                 @if($articleId->urlToImage != null)
                    <img height="500" src="{{$articleId->urlToImage}}" class="card-img" alt="{{$articleId->title}}">
                @elseif($articleId->urlToImage === null)
                <img height="500" src="../img/No_image_available.png" class="card-img" alt="{{$articleId->title}}">
                @else
                <img height="500" src="../{{$articleId->img}}" class="card-img" alt="{{$articleId->title}}">
                @endif
                <div class="row">
                    <div class="col">
                        <h3 class="ms-5" style="font-weight: 700">{{$articleId->title}}</h3>
                    </div>
                </div>
        </div>
        <div class="row">
                    <div id="title_clr" class="col">
                        @if($articleId->author != null)
                        <h3 class="ms-5" style="font-weight: 700">Author : </h3> <p class="ms-5">{{$articleId->author}}</p>
                        @else
                        <h3 class="ms-5" style="font-weight: 700">Author : </h3><p class="ms-5">Unknown Author</p>
                        @endif    
                    </div>
                </div>
                <div class="row">
                    <div id="title_clr" class="col">
                        @if($articleId->description != null)
                        <h3 class="ms-5" style="font-weight: 700">Description : </h3> <p class="ms-5">{{$articleId->description}}</p>
                        @else
                        <h3 class="ms-5" style="font-weight: 700">Description : </h3> <p class="ms-5">No description</p>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div id="title_clr" class="col">
                        @if($articleId->content != null)
                        <h3 class="ms-5" style="font-weight: 700">Content : </h3> <p class="ms-5">{{$articleId->content}}</p>
                        @else
                        <h3 class="ms-5" style="font-weight: 700">Content : </h3> <p class="ms-5">No content</p>
                        @endif
                    </div>
                    <div class="row">
                        <div id="title_clr" class="col">
                            <h3 class="ms-5" style="font-weight: 700">Full article <a class="btn btn-success" href="{{url($articleId->url)}}">Here</a>
                        </div>
                </div>
                <div class="row">
                    <div id="title_clr" class="col">
                        <h3 class="ms-5" style="font-weight: 700">Published date : </h3> <p class="ms-5">{{$articleId->publishedAt}}</p>
                    </div>
                </div>
                <div class="row">
                    <div id="title_clr" class="col">
                        <h3 class="ms-5" style="font-weight: 700">Delete Article : </h3>
                        <form method="post" action="{{url('api/article_delete',$articleId->id)}}" >
                            @csrf
                            @method('delete')
                            <button onclick="return confirm('Are you sure delete {{$articleId->title}}')" class="btn btn-danger ms-5" type="submit" value="Delete" >Delete</button>
                            </form> 
                    </div>
                </div>
                <div class="row">
                    <div id="title_clr" class="col">
                        <h3 class="ms-5" style="font-weight: 700">Update Article <a class="btn btn-primary" href="{{url('update_article', $articleId->id)}}">Update</a>
                    </div>
            </div>