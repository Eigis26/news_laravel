 <div id="bg_title" class="row">
        <div class="col pb-5">
            <h3 id="title_clr" class="ms-5" style="font-weight: 700">Blog posts</h3>
        </div>
    </div>
    <div id="bg_title" class="row">
        <div style="justify-content: center" class="cards-over">
            <div class="slider">
        <?php $count = 0; ?>
        @foreach($article as $posts)
        <?php
        if($count == 6) break; ?>
            <div class="cards-bg">
                <div class="card bg-dark text-white">
                    <div class="slide">
                    @if($posts->urlToImage != null)
                   <img src="{{$posts->urlToImage}}" class="card-img" alt="..." width="250" height="200">
                   @elseif($posts->urlToImage === null) 
                   <img src="img/No_image_available.png" class="card-img" alt="..." width="250" height="200">

                   @else
                   <img src="{{$posts->img}}" class="card-img" alt="..." width="250" height="200">
                   @endif
                    <div class="card-img-overlay">
                        <div class="blogs_title_deg">
                            <a style="text-decoration: none;" href="{{url('show_article', $posts->id)}}">     
                                <h6 style="margin-top: 30%; padding: 2px; color:white" class="card-text">{{$posts->title}}</h6></a>
                        </div>
                    </div>
                </div>
                  </div>
            </div>
        <?php $count ++; ?>
        @endforeach
            </div>
        </div>
    </div>
