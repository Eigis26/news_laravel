<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'index']);
Route::get('/blog/{id}', [PostController::class, 'blog']);
Route::get('/search', [PostController::class, 'search']);
Route::get('/search_articles', [PostController::class, 'search_articles']);
Route::get('/create_article', [PostController::class, 'create_article']);
Route::get('/mode', [PostController::class, 'mode']);
Route::get('/mode_on', [PostController::class, 'mode_on']);
Route::get('/update_article/{id}', [PostController::class, 'update_article']);
Route::get('/show_article/{id}', [PostController::class, 'show_article']);
Route::put('/article/{id}', [PostController::class, 'update']);
Route::post('/article', [PostController::class, 'store']);


