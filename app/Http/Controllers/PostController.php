<?php

namespace App\Http\Controllers;

use App\Models\Articles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Response;

class PostController extends Controller
{
    public function index(Request $request)
    {

        $api_url= 'https://newsapi.org/v2/everything?q=apple&from=2022-10-06&to=2022-10-06&sortBy=popularity&apiKey=5ec18aa6178a455bac3cc0ceb043ed08';
        $response = Http::get($api_url);
        $data = json_decode($response)->articles;
        foreach($data as $post)
        { 
            $post=(array)$post;
                 Articles::updateOrCreate(
                 ['author' => $post['author'],
                 'title' => $post['title'],
                 'description' => $post['description'],
                 'url' => $post['url'],
                 'urlToImage' => $post['urlToImage'],
                 'publishedAt' => $post['publishedAt'],
                 'content' => $post['content']]
                );
            }
        $article=Articles::orderBy('id', 'DESC')->get();
        return view('main', compact('article'));
    }
    public function search(Request $request)
    {
        
        $searchas=$request->search;
        
        $article=Articles::where('title', 'like', "%$searchas%")
        ->orwhere('content', 'like',"%$searchas%")
        ->orWhere('description', 'like', "%$searchas%")->get();

        return view('search_articles', compact('article'));
    }
    
    public function create_article()
    {
        return view('create_article');
    }

    public function mode ()
    {
        Cookie::queue('mode', 'dark', 120);
        return redirect()->back();
    }
    public function mode_on()
    {
        Cookie::queue('mode', 'dark', -1);
        return redirect()->back();

    }
    public function update_article ($id)
    {
        $data = Articles::findOrFail($id);
        return view('update_article', compact('data'));
    }

    public function show_article($id)
    {
        $articleId = Articles::findOrFail($id);
        return view('view_blog', compact('articleId'));
    }

    public function update (Request $request, $id)
    {
        $article =Articles::findOrFail($id);
        $date = date('Y-m-d H:i:s');
        $article -> author = $request -> author;
        $article -> title = $request -> title;
        $article -> description = $request -> description;
        $article -> url = $request -> url;
        $article -> publishedAt = $date;
        $article -> content = $request -> content;
        $image=$request->urlToImage;
        if($image)
        {
        $imagename='image'.'/'.time().'.'.$image->getClientOriginalExtension();
        $request->urlToImage->move('image', $imagename);
        $article->img=$imagename;
        }
        $article -> save();
        return redirect()->back()->with('message', 'Article updated successfully');
    }

    public function store(Request $request)
    {
        $article = new Articles;
        $date = date('Y-m-d H:i:s');
        $article -> author = $request -> author;
        $article -> title = $request -> title;
        $article -> description = $request -> description;
        $article -> url = $request -> url;
        $article -> publishedAt = $date;
        $article -> content = $request -> content;
        $image=$request->urlToImage;
        $imagename='image'.'/'.time().'.'.$image->getClientOriginalExtension();
        $request->urlToImage->move('image', $imagename);
        $article->img=$imagename;
        $article -> save();
        return redirect()->back()->with('message', 'Article created');
    }

   
}
