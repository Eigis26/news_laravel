<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ArticleController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::controller(ArticleController::class)->group(function () {

    Route::get('/articles', 'index');
    Route::get('/article/{id}', 'show');
    Route::post('/article', 'store');
    Route::put('/article/{id}', 'update');
    Route::delete('/article_delete/{id}', 'destroy');

});

